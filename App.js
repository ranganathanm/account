/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';

import {
  ActivityIndicator,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaView, SafeAreaProvider} from 'react-native-safe-area-context';
import auth from '@react-native-firebase/auth';
import Login from './src/screen/Login';
import LocalPassword from './src/screen/LocalPassword';
import SplashScreen from 'react-native-splash-screen';
import DrawerStack from './src/navigation/DrawerStack';
const Stack = createStackNavigator();

const App = () => {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const [localVerification, setlocalVerification] = useState(false);

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  const localVerificationHandler = () => {
    setlocalVerification(true);
  };
  useEffect(() => {
    SplashScreen.hide();
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing)
    return (
      <ActivityIndicator
        size="large"
        color="#17A44E"
        style={{
          position: 'absolute',
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1,
        }}
      />
    );

  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <StatusBar barStyle={'light-content'} />
        <SafeAreaView style={{flex: 1}}>
          <Stack.Navigator headerMode={'none'}>
            {!user ? (
              <Stack.Screen name="Login" component={Login} />
            ) : !localVerification ? (
              <Stack.Screen name="Home">
                {props => (
                  <LocalPassword
                    {...props}
                    localVerificationHandler={localVerificationHandler}
                  />
                )}
              </Stack.Screen>
            ) : (
              <Stack.Screen name="DrawerStack" component={DrawerStack} />
            )}
          </Stack.Navigator>
        </SafeAreaView>
      </SafeAreaProvider>
    </NavigationContainer>
  );
};

export default App;

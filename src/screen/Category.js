import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  Modal,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
  Alert,
} from 'react-native';
import Header from '../component/Header';
import database from '@react-native-firebase/database';
import {SCREEN_WIDTH} from '../shared/share';
import Icon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import Loader from '../component/Loader';
export default function Category() {
  const [catergoryList, setcatergoryList] = useState([]);
  const [addModal, setAddModal] = useState(false);
  const [catergoryName, setCatergoryName] = useState('');
  const [loading, setLoading] = useState(false);
  const [modalloading, setmodalLoading] = useState(false);
  const [updateData, setUpdateData] = useState(null);
  useEffect(() => {
    setLoading(true);
    const onValueChange = database()
      .ref(`category`)
      .orderByChild('status')
      .equalTo(1)
      .on('value', snapshot => {
        let tempData = [];
        if (!!snapshot.val()) {
          Object.entries(snapshot.val()).forEach(([key, value]) => {
            tempData.push({...value, id: key});
          });
        }
        setcatergoryList(tempData);
        setLoading(false);
      });

    // Stop listening for updates when no longer required
    return () => database().ref(`category`).off('value', onValueChange);
  }, []);

  const addCategoryHandler = () => {
    setmodalLoading(true);

    if (!!updateData) {
      let updatedata = {
        title: catergoryName,
        status: 1,
      };
      database()
        .ref(`/category/${updateData.id}`)
        .update({...updatedata})
        .then(() => {
          setUpdateData(null);
          setCatergoryName('');
          setmodalLoading(false);
        });
    } else {
      let data = {
        title: catergoryName,
        status: 1,
      };
      let newReference = database().ref('category').push();
      newReference
        .set({
          ...data,
        })
        .then(() => {
          setmodalLoading(false);
          setAddModal(false);
          setCatergoryName('');

          ToastAndroid.showWithGravity(
            'Sucessfully created',
            ToastAndroid.LONG,
            ToastAndroid.TOP,
          );
        })
        .catch(e => {
          setmodalLoading(false);

          ToastAndroid.showWithGravity(
            'Network error',
            ToastAndroid.LONG,
            ToastAndroid.TOP,
          );
        });
    }
  };

  const deleteHandler = data => {
    Alert.alert('', 'Are you sure want to delete this?', [
      {
        text: 'No',
        onPress: () => {},
      },
      {
        text: 'Yes',
        onPress: () => {
          let deletedata = {
            title: data.title,
            status: 0,
          };
          database()
            .ref(`/category/${data.id}`)
            .update({...deletedata})
            .then(() => {
              ToastAndroid.showWithGravity(
                'Successfully deleted!',
                ToastAndroid.LONG,
                ToastAndroid.TOP,
              );
            });
        },
      },
    ]);
  };
  const renderItem = ({item}) => (
    <View
      style={{
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#fff',
        shadowColor: '#999',
        elevation: 8,
        borderRadius: 2,
      }}>
      <Text style={{fontSize: 16, fontWeight: 'bold', color: '#888'}}>
        {item.title}
      </Text>

      <TouchableOpacity
        style={{marginLeft: 'auto', marginRight: 5}}
        onPress={() => {
          setCatergoryName(item.title);
          setUpdateData({...item});
        }}>
        <Text>
          <Icon name="edit" style={{fontSize: 20, color: '#777'}}></Icon>
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          deleteHandler({...item});
        }}>
        <Text>
          <MIcon
            name="delete-outline"
            style={{fontSize: 20, color: 'orangered'}}></MIcon>
        </Text>
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={{flex: 1}}>
      <Header title="Catergory" />
      <View
        style={{
          position: 'absolute',
          zIndex: 1,
          top: 0,
          justifyContent: 'center',
          right: 10,
          height: 50,
          alignItems: 'center',
          paddingHorizontal: 5,
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => {
            setAddModal(true);
          }}>
          <Icon name="plus" style={{color: '#fff', fontSize: 19}}></Icon>
        </TouchableOpacity>
      </View>
      {loading && <Loader />}
      <FlatList
        data={catergoryList}
        renderItem={renderItem}
        keyExtractor={(t, i) => i}
        style={{paddingHorizontal: 20}}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{}}
        onEndReachedThreshold={0.5}
        onEndReached={() => {}}
      />
      <Modal
        animationType="fade"
        transparent={true}
        visible={addModal || !!updateData}
        onRequestClose={() => {
          setAddModal(false);
          setUpdateData(null);
          setCatergoryName('');
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
            justifyContent: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              width: SCREEN_WIDTH - 40,
              padding: 20,
              borderRadius: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                Category update
              </Text>
              <TouchableOpacity
                style={{alignItems: 'center', justifyContent: 'center'}}
                onPress={() => {
                  setAddModal(false);
                  setUpdateData(null);
                  setCatergoryName('');
                }}>
                <Icon name="close" style={{fontSize: 16, color: '#777'}}></Icon>
              </TouchableOpacity>
            </View>

            <Text style={styles.label}>Title</Text>
            <TextInput
              value={catergoryName}
              placeholder=""
              style={styles.textInput}
              onChangeText={val => {
                setCatergoryName(val);
              }}
            />
            <View style={{marginBottom: 10}}></View>
            <Button
              title="save"
              disabled={catergoryName === '' || modalloading}
              onPress={() => {
                addCategoryHandler();
              }}></Button>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  label: {
    marginTop: 10,
    fontWeight: 'bold',
    color: '#999',
  },
  textInput: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 5,
    color: '#555',
    padding: 3,
    paddingHorizontal: 5,
  },
  errorMes: {
    color: 'red',
  },
});

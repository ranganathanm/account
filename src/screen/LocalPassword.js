import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Button,
  ActivityIndicator,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
} from 'react-native';
import {retrieveUserSession, storeUserSession} from '../shared/SecureStore';

export default function LocalPassword({localVerificationHandler}) {
  const [loading, setloading] = useState(true);
  const [localPassword, setlocalPassword] = useState(null);
  const [enterPass, setenterpass] = useState('');
  useEffect(async () => {
    try {
      const data = await retrieveUserSession('user_data');

      const tempData = JSON.parse(data);
      setlocalPassword(tempData?.localpin || null);
      setloading(false);
    } catch (error) {}
  }, []);
  useEffect(async () => {
    if (enterPass.length === 4) {
      setloading(true);
      if (!!localPassword && enterPass === localPassword) {
        localVerificationHandler();
      } else if (localPassword === null) {
        const storeRes = await storeUserSession('user_data', {
          localpin: enterPass,
        });

        localVerificationHandler();
      } else {
        setloading(false);
        setenterpass('');
        ToastAndroid.showWithGravity(
          'your pin is incorrect.',
          ToastAndroid.LONG,
          ToastAndroid.TOP,
        );
      }
    }
  }, [enterPass]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '',
        paddingHorizontal: 20,
        justifyContent: 'space-between',
      }}>
      {loading && (
        <ActivityIndicator
          size="large"
          color="#2196F3"
          style={{
            position: 'absolute',
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: 1,
          }}
        />
      )}
      <View style={{marginTop: 50, alignItems: 'center'}}>
        <Text style={{fontSize: 20, marginVertical: 20}}>Enter your PIN</Text>
        <View
          style={{
            flexDirection: 'row',
            width: 180,
            alignSelf: 'center',

            justifyContent: 'space-between',
          }}>
          {[...Array(4)].map((e, i) => {
            return (
              <View
                key={i}
                style={{
                  width: 15,
                  height: 15,
                  borderWidth: 1,
                  borderColor: '#000',
                  borderRadius: 15,
                  backgroundColor: i < enterPass.length ? '#000' : '#fff',
                }}></View>
            );
          })}
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'flex-end',
        }}>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 0].map((ele, ind) => {
          return (
            <View
              style={{
                width: '33.33%',
                alignItems: 'center',
                justifyContent: 'center',
                marginVertical: 5,
              }}
              key={ind}>
              <TouchableOpacity
                style={{
                  width: 50,
                  height: 50,
                  borderWidth: 2,
                  borderColor: '#999',
                  borderRadius: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => {
                  if (enterPass.length < 4) setenterpass(p => p + ele);
                }}>
                <Text style={{fontSize: 16, fontWeight: 'bold', color: '#999'}}>
                  {ele}
                </Text>
              </TouchableOpacity>
            </View>
          );
        })}
        <View
          style={{
            width: '33.33%',
            alignItems: 'center',
            justifyContent: 'center',
            marginVertical: 5,
          }}
          key={'X'}>
          <TouchableOpacity
            style={{
              width: 50,
              height: 50,
              borderWidth: 2,
              borderColor: '#999',
              borderRadius: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => {
              setenterpass(p => p.slice(0, -1));
            }}>
            <View
              style={{
                borderWidth: 1,
                width: 0,
                height: 0,
              }}></View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Button,
  ToastAndroid,
  FlatList,
  Modal,
  Alert,
  ImageBackground,
} from 'react-native';
import Header from '../component/Header';
import Loader from '../component/Loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import database from '@react-native-firebase/database';
import RBSheet from 'react-native-raw-bottom-sheet';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {SwipeListView, SwipeRow} from 'react-native-swipe-list-view';
import {SCREEN_WIDTH} from '../shared/share';
import AddCreditDebit from './AddCreditDebit';
import _ from 'lodash';
import MIcon from 'react-native-vector-icons/MaterialIcons';

const colorArray = ['#ff8482', '#e6a8ff', '#66d990', '#77d0fc', 'orange'];

export default function HomeScreen() {
  const totalRef = useRef();
  const timerRef = useRef();
  const [loading, setLoading] = useState(false);
  const [transactionList, setTractionList] = useState([]);
  const [totalData, setTotalData] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [catergoryList, setcatergoryList] = useState(null);
  const [updateData, setupdateData] = useState(null);
  const [filterData, setFilterData] = useState({
    fromDate: moment().startOf('month').format('YYYY-MM-DD'),
    toDate: moment().endOf('month').format('YYYY-MM-DD'),
    catData: null,
    cd_type: null,
  });
  const [filterModal, setfilterModal] = useState(false);
  useEffect(() => {
    database()
      .ref(`category`)
      .orderByChild('status')
      .equalTo(1)
      .once('value')
      .then(snapshot => {
        let tempData = [];
        if (!!snapshot.val()) {
          Object.entries(snapshot.val()).forEach(([key, value]) => {
            tempData.push({...value, id: key});
          });
        }
        setcatergoryList(tempData);

        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
      });

    // Stop listening for updates when no longer required
  }, []);

  useEffect(() => {
    getData();

    return () => {
      clearTimeout(timerRef.current);
    };
  }, []);

  const getData = (tempFilter = filterData) => {
    setTotalData(null);
    setLoading(true);

    database()
      .ref(`transactionData`)
      .orderByChild('cd_date')
      .startAt(tempFilter.fromDate)
      .endAt(tempFilter.toDate)
      .once('value')
      .then(snapshot => {
        let tempData = [];
        let ctot = 0;
        let dtot = 0;

        if (!!snapshot.val()) {
          Object.entries(snapshot.val()).forEach(([key, value]) => {
            tempData.push({...value, id: key});
          });

          tempData = _.orderBy(tempData, ['cd_date'], ['asc']);
          if (!!tempFilter.catData)
            tempData = _.filter(tempData, {cat_id: tempFilter?.catData?.id});
          if (tempFilter.cd_type !== null)
            tempData = _.filter(tempData, {cd_type: tempFilter?.cd_type});
          tempData.forEach(ele => {
            if (ele.cd_type === 1) {
              ctot = ctot + parseInt(ele.amount);
            } else {
              dtot = dtot + parseInt(ele.amount);
            }
          });
          setTotalData({ctot, dtot});
          setTractionList(tempData);
        } else {
          setTractionList([]);
        }

        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
      });
  };

  const deleteHandler = id => {
    Alert.alert('', 'Are you sure want to delete this?', [
      {
        text: 'No',
        onPress: () => {},
      },
      {
        text: 'Yes',
        onPress: () => {
          database()
            .ref(`/transactionData/${id}`)
            .remove()
            .then(() => {
              getData();
              ToastAndroid.showWithGravity(
                'Successfully deleted!',
                ToastAndroid.LONG,
                ToastAndroid.TOP,
              );
            })
            .catch(() => {
              ToastAndroid.showWithGravity(
                'Oops! Something went wrong.',
                ToastAndroid.LONG,
                ToastAndroid.TOP,
              );
            });
        },
      },
    ]);
  };
  const ColorHandler = (val, i) => {
    //65-90
    const index = val.charCodeAt(0) % 5;
    return colorArray[index];
  };
  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          backgroundColor: '#fffff9',
          padding: 5,
          flexDirection: 'row',
          alignItems: 'center',
          height: 50,
          marginVertical: 5,
        }}>
        <View
          style={{
            width: 35,
            height: 35,
            borderRadius: 35,
            backgroundColor: ColorHandler(
              item.title.toUpperCase().charAt(0),
              index,
            ),
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 15,
          }}>
          <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 18}}>
            {item.title.charAt(0)}
          </Text>
        </View>
        <View style={{marginLeft: 10}}>
          <Text style={{fontSize: 16, color: '#555'}}>{item.title}</Text>
          <Text style={{fontSize: 12, color: '#555'}}>
            {moment(item.cd_date).format('DD MMM YY')}
          </Text>
        </View>

        <Text
          style={{
            marginLeft: 'auto',
            fontSize: 18,
            color: item.cd_type === 0 ? '#ff2929' : '#17A44E',
            marginRight: 15,
          }}>
          {item.cd_type === 0 ? '-' : '+'}
          {item.amount}
        </Text>
      </View>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fffff9'}}>
      <Header title="Home" />
      <View
        style={{
          position: 'absolute',
          zIndex: 1,
          top: 0,
          justifyContent: 'center',
          right: 10,
          height: 50,
          alignItems: 'center',
          paddingHorizontal: 5,
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => {
            setfilterModal(true);
          }}
          style={{marginRight: 15}}
          activeOpacity={0.7}>
          <Icon name="filter" style={{color: '#fff', fontSize: 19}}></Icon>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(true);
          }}>
          <Icon name="plus" style={{color: '#fff', fontSize: 19}}></Icon>
        </TouchableOpacity>
      </View>
      {(loading || catergoryList === null) && <Loader />}
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingHorizontal: 15,
          }}>
          <Text style={styles.label}>
            {moment(filterData.fromDate).format('DD-MMM-YYYY')}
            {'  to  '}
          </Text>
          <Text style={styles.label}>
            {moment(filterData.toDate).format('DD-MMM-YYYY')}
          </Text>
        </View>

        {/* <View style={{alignItems: 'center'}}>
          <Text style={{}}>{filterData?.catData?.title || ''}</Text>
        </View> */}
      </View>

      <SwipeListView
        data={transactionList}
        renderItem={renderItem}
        renderHiddenItem={(data, rowMap) => (
          <View
            style={{
              //  backgroundColor: '#fffff9',
              flexDirection: 'row',
              alignItems: 'center',
              height: 50,
              marginVertical: 5,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={{
                width: 70,
                height: '100%',
                backgroundColor: '#2194f3',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                setupdateData({...data.item});
                setModalVisible(true);
              }}>
              <Icon name="edit" style={{fontSize: 20, color: '#fff'}}></Icon>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 70,
                height: '100%',
                backgroundColor: '#ff2929',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                deleteHandler(data.item.id);
              }}>
              <MIcon
                name="delete-outline"
                style={{fontSize: 20, color: '#fff'}}></MIcon>
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(t, i) => i + 1}
        leftOpenValue={80}
        rightOpenValue={-80}
        swipeToOpenPercent={80}
        stopLeftSwipe={90}
        stopRightSwipe={-90}
        disableRightSwipe={false}
        style={{flex: 1}}
        closeOnScroll={true}
        closeOnRowOpen={true}
        closeOnRowPress={true}
        swipeToOpenVelocityContribution={10}
      />

      <View
        style={{
          position: 'absolute',
          bottom: 0,
        }}></View>

      <SwipeRow
        ref={totalRef}
        leftOpenValue={SCREEN_WIDTH}
        rightOpenValue={-SCREEN_WIDTH}
        leftActivationValue={30}
        leftActionValue={SCREEN_WIDTH}
        rightActivationValue={30}
        rightActionValue={-SCREEN_WIDTH}
        stopRightSwipe={0}
        onRowDidOpen={() => {
          timerRef.current = setTimeout(() => {
            totalRef.current.closeRow();
          }, 3000);
        }}>
        <TouchableOpacity
          activeOpacity={1}
          style={{height: 50}}
          onPress={() => {
            clearTimeout(timerRef.current);
            totalRef.current.closeRow();
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: '#2194f3',
              justifyContent: 'center',
              height: 50,
            }}>
            <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>
              <Icon name="rupee" style={{color: '#fff', fontSize: 19}}></Icon>{' '}
              {totalData?.dtot &&
                totalData?.ctot &&
                totalData.ctot - totalData?.dtot}
            </Text>
          </View>
        </TouchableOpacity>
        <View
          style={{flexDirection: 'row', height: 50, backgroundColor: 'red'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: '#ff2929',
              justifyContent: 'center',
              height: 50,
            }}>
            <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>
              <Icon name="rupee" style={{color: '#fff', fontSize: 19}}></Icon>{' '}
              {totalData?.dtot || ''}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: '#17A44E',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>
              <Icon name="rupee" style={{color: '#fff', fontSize: 19}}></Icon>{' '}
              {totalData?.ctot || ''}
            </Text>
          </View>
        </View>
      </SwipeRow>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
        style={{}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
            justifyContent: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              width: SCREEN_WIDTH - 40,

              padding: 20,
              borderRadius: 10,
            }}>
            <AddCreditDebit
              catergoryList={catergoryList}
              closeHandler={res => {
                setupdateData(null);
                setModalVisible(false);
                if (res === 1) {
                  getData();
                }
              }}
              updateData={updateData}
            />
          </View>
        </View>
      </Modal>
      <Modal
        animationType="fade"
        transparent={true}
        visible={filterModal}
        onRequestClose={() => {
          setfilterModal(false);
        }}
        style={{}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
            justifyContent: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              width: SCREEN_WIDTH - 40,

              padding: 20,
              borderRadius: 10,
            }}>
            <FilterScreen
              catergoryList={catergoryList}
              closeHandler={(res = 0, data = null) => {
                setfilterModal(false);
                if (res === 1) {
                  setFilterData({...data});
                  getData({...data});
                }
              }}
              filterData={filterData}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const FilterScreen = ({closeHandler, filterData, catergoryList}) => {
  const refRBSheet = useRef();

  const [search, setsearch] = useState('');
  const [dateModal, setDateModal] = useState(null);
  const [tempfilterData, settempFilterData] = useState({...filterData});

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 16}}>Filter</Text>
        <TouchableOpacity
          style={{alignItems: 'center', justifyContent: 'center'}}
          onPress={() => {
            closeHandler();
          }}>
          <Icon name="close" style={{fontSize: 16, color: '#777'}}></Icon>
        </TouchableOpacity>
      </View>
      <Text style={styles.label}>From Date </Text>

      <TouchableOpacity
        onPress={() => {
          setDateModal('fromDate');
        }}>
        <TextInput
          placeholder=""
          placeholderTextColor="#555"
          style={[styles.textInput]}
          value={
            !!tempfilterData?.fromDate
              ? moment(tempfilterData.fromDate).format('DD-MM-YYYY')
              : ''
          }
          editable={false}
        />
      </TouchableOpacity>
      <Text style={styles.label}>To Date </Text>
      <TouchableOpacity
        onPress={() => {
          setDateModal('toDate');
        }}>
        <TextInput
          placeholder=""
          placeholderTextColor="#555"
          style={[styles.textInput]}
          value={
            !!tempfilterData?.toDate
              ? moment(tempfilterData.toDate).format('DD-MM-YYYY')
              : ''
          }
          editable={false}
        />
      </TouchableOpacity>
      {!!dateModal && (
        <DateTimePicker
          value={moment(tempfilterData[dateModal]).toDate()}
          onChange={val => {
            let type = dateModal;
            setDateModal(false);
            if (val.type === 'set') {
              settempFilterData(p => ({
                ...p,
                [type]: moment(val.nativeEvent.timestamp).format('YYYY-MM-DD'),
              }));
            }
          }}
          is24Hour={true}
          display="spinner"
          mode={'date'}
        />
      )}

      <Text style={styles.label}>Type</Text>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => {
            settempFilterData(p => ({
              ...p,
              cd_type: p.cd_type === 1 ? null : 1,
            }));
          }}>
          <View style={[styles.radiobutton, {}]}>
            {tempfilterData?.cd_type === 1 && (
              <View
                style={{
                  margin: 2,
                  backgroundColor: '#17A44E',
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                }}></View>
            )}
          </View>
          <Text
            style={{color: tempfilterData?.cd_type === 1 ? '#17A44E' : '#999'}}>
            Credit
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => {
            settempFilterData(p => ({
              ...p,
              cd_type: p.cd_type === 0 ? null : 0,
            }));
          }}>
          <View style={[styles.radiobutton]}>
            {tempfilterData?.cd_type === 0 && (
              <View
                style={{
                  margin: 2,
                  backgroundColor: '#ff2929',
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                }}></View>
            )}
          </View>
          <Text
            style={{color: tempfilterData?.cd_type === 0 ? '#ff2929' : '#999'}}>
            Debit
          </Text>
        </TouchableOpacity>
      </View>

      <Text style={styles.label}>Category </Text>

      <TouchableOpacity
        onPress={() => {
          refRBSheet.current.open();
        }}>
        <TextInput
          placeholder=""
          style={styles.textInput}
          value={tempfilterData?.catData?.title || ''}
          editable={false}
        />

        {!!tempfilterData?.catData && (
          <TouchableOpacity
            style={{
              position: 'absolute',
              width: 30,
              height: '100%',
              right: 0,
              // bottom: 3,
              // backgroundColor: '#999',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => {
              settempFilterData(p => ({...p, catData: null}));
            }}>
            <Icon name="close" style={{color: '#555', fontSize: 14}}></Icon>
          </TouchableOpacity>
        )}
      </TouchableOpacity>
      <View style={{marginBottom: 20}}></View>
      <Button
        title="filter"
        onPress={() => {
          closeHandler(1, tempfilterData);
        }}></Button>

      <RBSheet
        animationType={'fade'}
        openDuration={250}
        closeDuration={250}
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={400}
        customStyles={{
          container: {
            borderTopEndRadius: 40,
            borderTopStartRadius: 40,
            paddingHorizontal: 10,
            paddingBottom: 0,
            backgroundColor: '#fff',
          },
        }}>
        <TouchableOpacity style={{flex: 1, padding: 20}} activeOpacity={1}>
          <ScrollView
            style={{flex: 1}}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps={'handled'}>
            <Text style={{fontWeight: 'bold', color: '#999', marginBottom: 10}}>
              Select Catergory
            </Text>
            <View>
              <TextInput
                label=""
                onChangeText={val => {
                  setsearch(val);
                }}
                placeholder="Search"
                value={search}
                style={[styles.textInput]}
              />
              <View
                style={{
                  position: 'absolute',
                  width: 30,
                  height: '100%',
                  right: 0,
                  //bottom: 3,
                  // backgroundColor: '#999',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="search"
                  style={{color: '#555', fontSize: 14}}></Icon>
              </View>
            </View>
            {catergoryList.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    //marginVertical: 5,
                    borderBottomWidth: 2,
                    borderColor: '#E7EAF0',
                    padding: 2,
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 10,
                    display: item.title
                      .toLowerCase()
                      .includes(search.toLowerCase())
                      ? 'flex'
                      : 'none',
                  }}
                  activeOpacity={0.9}
                  onPress={() => {
                    settempFilterData(p => ({...p, catData: {...item}}));
                    refRBSheet.current.close();
                  }}>
                  <Text style={{color: '#474747'}} numberOfLines={1}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              );
            })}

            <View style={{marginBottom: 5, alignItems: 'center'}}>
              {catergoryList.filter(e =>
                e.title.toLowerCase().includes(search.toLowerCase()),
              ).length === 0 && (
                <Text style={[styles.label]}>No data found</Text>
              )}
            </View>
          </ScrollView>
        </TouchableOpacity>
      </RBSheet>
    </View>
  );
};
const styles = StyleSheet.create({
  label: {
    marginTop: 10,
    fontWeight: 'bold',
    color: '#999',
  },
  textInput: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 5,
    color: '#555',
    padding: 3,
    paddingHorizontal: 5,
  },
  radiobutton: {
    width: 20,
    height: 20,
    borderRadius: 20,
    padding: 2,
    backgroundColor: '#fff',
    borderWidth: 2,
    borderColor: '#999',
    marginHorizontal: 5,
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

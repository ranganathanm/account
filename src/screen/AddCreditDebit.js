import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Button,
  ToastAndroid,
} from 'react-native';
import Header from '../component/Header';
import Loader from '../component/Loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import database from '@react-native-firebase/database';
import RBSheet from 'react-native-raw-bottom-sheet';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

const addSchema = Yup.object().shape({
  title: Yup.string().required('Title required'),
  cat_id: Yup.string(),
  cd_type: Yup.number().typeError('').integer('').required(''),
  amount: Yup.number()
    .typeError('Enter valid amount')
    .integer('Enter valid amount')
    .positive('Enter valid amount')
    .required('Amount required'),
  cd_date: Yup.string().required('Select date'),
});

export default function AddCreditDebit({
  closeHandler,
  catergoryList,
  updateData,
}) {
  const refRBSheet = useRef();

  const [loading, setLoading] = useState(false);

  const [search, setsearch] = useState('');
  const [dateModal, setDateModal] = useState(false);

  const formik = useFormik({
    initialValues: {
      title: updateData?.title || '',
      cat_id: updateData?.cat_id || '',
      cd_type: !!updateData ? updateData.cd_type : 1,
      amount: updateData?.amount || '',
      cd_date: updateData?.cd_date || moment().format('YYYY-MM-DD'),
    },
    validateOnChange: false,
    validationSchema: addSchema,
    onSubmit: values => {
      setLoading(true);
      if (!!updateData) {
        let tempupdatedata = {
          title: values.title,
          cat_id: values.cat_id,
          cd_type: values.cd_type,
          amount: values.amount,
          cd_date: values.cd_date,
        };
        database()
          .ref(`/transactionData/${updateData.id}`)
          .update({...tempupdatedata})
          .then(() => {
            formik.resetForm();
            setLoading(false);
            closeHandler(1);

            ToastAndroid.showWithGravity(
              'Sucessfully Updated',
              ToastAndroid.LONG,
              ToastAndroid.TOP,
            );
          })
          .catch(e => {
            setLoading(false);

            ToastAndroid.showWithGravity(
              'Network error',
              ToastAndroid.LONG,
              ToastAndroid.TOP,
            );
          });
      } else {
        let data = {
          title: values.title,
          cat_id: values.cat_id,
          cd_type: values.cd_type,
          amount: values.amount,
          cd_date: values.cd_date,
        };
        let newReference = database().ref('transactionData').push();
        newReference
          .set({
            ...data,
          })
          .then(() => {
            setLoading(false);
            closeHandler(1);

            ToastAndroid.showWithGravity(
              'Sucessfully created',
              ToastAndroid.LONG,
              ToastAndroid.TOP,
            );
            formik.resetForm();
          })
          .catch(e => {
            setLoading(false);

            ToastAndroid.showWithGravity(
              'Network error',
              ToastAndroid.LONG,
              ToastAndroid.TOP,
            );
          });
      }
    },
  });

  return (
    <View>
      {loading && <Loader />}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 16}}>New transaction</Text>
        <TouchableOpacity
          style={{alignItems: 'center', justifyContent: 'center'}}
          onPress={() => {
            closeHandler();
          }}>
          <Icon name="close" style={{fontSize: 16, color: '#777'}}></Icon>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.label}>Title</Text>
        <View>
          <View>
            <TextInput
              value={''}
              placeholder=""
              style={styles.textInput}
              value={formik.values.title}
              onChangeText={val => {
                formik.setFieldValue('title', val);
                formik.setFieldValue('cat_id', '');
              }}
            />
            <TouchableOpacity
              style={{
                position: 'absolute',
                width: 30,
                height: '100%',
                right: 0,
                bottom: 3,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                refRBSheet.current.open();
              }}>
              <Icon
                name="sort-down"
                style={{color: '#555', fontSize: 14}}></Icon>
            </TouchableOpacity>
          </View>
          {formik.errors.title ? (
            <Text style={styles.errorText}>{formik.errors.title}</Text>
          ) : null}
        </View>
        <Text style={styles.label}>Type</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => {
              formik.setFieldValue('cd_type', 1);
            }}>
            <View style={[styles.radiobutton, {}]}>
              {formik.values.cd_type === 1 && (
                <View
                  style={{
                    margin: 2,
                    backgroundColor: '#17A44E',
                    width: 10,
                    height: 10,
                    borderRadius: 10,
                  }}></View>
              )}
            </View>
            <Text
              style={{color: formik.values.cd_type === 1 ? '#17A44E' : '#999'}}>
              Credit
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => {
              formik.setFieldValue('cd_type', 0);
            }}>
            <View style={[styles.radiobutton]}>
              {formik.values.cd_type === 0 && (
                <View
                  style={{
                    margin: 2,
                    backgroundColor: '#ff2929',
                    width: 10,
                    height: 10,
                    borderRadius: 10,
                  }}></View>
              )}
            </View>
            <Text
              style={{color: formik.values.cd_type === 0 ? '#ff2929' : '#999'}}>
              Debit
            </Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.label}>Date </Text>
        <View>
          <TouchableOpacity
            onPress={() => {
              setDateModal(true);
            }}>
            <TextInput
              placeholder=""
              placeholderTextColor="#555"
              style={[styles.textInput]}
              value={
                !!formik.values.cd_date
                  ? moment(formik.values.cd_date).format('DD-MM-YYYY')
                  : ''
              }
              editable={false}
            />
          </TouchableOpacity>
        </View>

        {!!dateModal && (
          <DateTimePicker
            value={moment(formik.values.cd_date).toDate()}
            onChange={val => {
              setDateModal(false);
              if (val.type === 'set') {
                formik.setFieldValue(
                  'cd_date',
                  moment(val.nativeEvent.timestamp).format('YYYY-MM-DD'),
                );
              }
            }}
            is24Hour={true}
            display="spinner"
            mode={'date'}
          />
        )}

        <Text style={styles.label}>Amount</Text>
        <View>
          <TextInput
            placeholder=""
            style={[
              styles.textInput,
              {paddingLeft: 25, fontWeight: 'bold', color: '#555'},
            ]}
            onChangeText={formik.handleChange('amount')}
            value={formik.values.amount}
            keyboardType="number-pad"
          />
          <View
            style={{
              position: 'absolute',
              width: 30,
              height: '100%',
              // backgroundColor: '#999',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon name="rupee" style={{color: '#555', fontSize: 14}}></Icon>
          </View>
        </View>
        {formik.errors.amount ? (
          <Text style={styles.errorText}>{formik.errors.amount}</Text>
        ) : null}
        <View style={{marginBottom: 20}}></View>
        <Button
          title="Submit"
          disabled={loading}
          onPress={formik.handleSubmit}></Button>
      </View>
      <RBSheet
        animationType={'fade'}
        openDuration={250}
        closeDuration={250}
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={400}
        customStyles={{
          container: {
            borderTopEndRadius: 40,
            borderTopStartRadius: 40,
            paddingHorizontal: 10,
            paddingBottom: 0,
            backgroundColor: '#fff',
          },
        }}>
        <TouchableOpacity style={{flex: 1, padding: 20}} activeOpacity={1}>
          <ScrollView
            style={{flex: 1}}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps={'handled'}>
            <Text style={{fontWeight: 'bold', color: '#999', marginBottom: 10}}>
              Select Catergory
            </Text>
            <View>
              <TextInput
                label=""
                onChangeText={val => {
                  setsearch(val);
                }}
                placeholder="Search"
                value={search}
                style={[styles.textInput]}
              />
              <View
                style={{
                  position: 'absolute',
                  width: 30,
                  height: '100%',
                  right: 0,
                  //bottom: 3,
                  // backgroundColor: '#999',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="search"
                  style={{color: '#555', fontSize: 14}}></Icon>
              </View>
            </View>
            {catergoryList.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    //marginVertical: 5,
                    borderBottomWidth: 2,
                    borderColor: '#E7EAF0',
                    padding: 2,
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 10,
                    display: item.title
                      .toLowerCase()
                      .includes(search.toLowerCase())
                      ? 'flex'
                      : 'none',
                  }}
                  activeOpacity={0.9}
                  onPress={() => {
                    formik.setFieldValue('title', item.title);
                    formik.setFieldValue('cat_id', item.id);
                    refRBSheet.current.close();
                  }}>
                  <Text style={{color: '#474747'}} numberOfLines={1}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              );
            })}

            <View style={{marginBottom: 5, alignItems: 'center'}}>
              {catergoryList.filter(e =>
                e.title.toLowerCase().includes(search.toLowerCase()),
              ).length === 0 && (
                <Text style={[styles.label]}>No data found</Text>
              )}
            </View>
          </ScrollView>
        </TouchableOpacity>
      </RBSheet>
    </View>
  );
}

const styles = StyleSheet.create({
  label: {
    marginTop: 10,
    fontWeight: 'bold',
    color: '#999',
  },
  textInput: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 5,
    color: '#555',
    padding: 3,
    paddingHorizontal: 5,
  },
  errorText: {
    color: 'red',
  },
  radiobutton: {
    width: 20,
    height: 20,
    borderRadius: 20,
    padding: 2,
    backgroundColor: '#fff',
    borderWidth: 2,
    borderColor: '#999',
    marginHorizontal: 5,
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

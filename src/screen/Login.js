import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  ActivityIndicator,
  Keyboard,
  ToastAndroid,
  Image,
} from 'react-native';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import auth from '@react-native-firebase/auth';
import Loader from '../component/Loader';
const SigninSchema = Yup.object().shape({
  username: Yup.string()
    .required('Username cannot be blank.')
    .email('Enter valid username'),
  password: Yup.string().required('Password cannot be blank.'),
});

export default function Login() {
  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: SigninSchema,
    onSubmit: values => {
      Keyboard.dismiss();
      if (!loading) {
        setLoading(true);
        auth()
          .signInWithEmailAndPassword(values.username, values.password)
          .then(() => {
            setLoading(false);
          })
          .catch(error => {
            setLoading(false);
            ToastAndroid.showWithGravity(
              'Invalid username or password',
              ToastAndroid.LONG,
              ToastAndroid.TOP,
            );
          });
      }
    },
  });
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fffff9',
        paddingHorizontal: 20,
        justifyContent: 'center',
      }}>
      {loading && <Loader />}
      <View
        style={{
          backgroundColor: '#fff',
          shadowColor: '#999',
          elevation: 8,
          padding: 20,
          borderRadius: 20,
        }}>
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../Img/logo.png')}
            style={{width: 100, height: 100}}
          />
        </View>
        <View style={{marginTop: 0}}>
          <Text style={styles.label}>Username</Text>
          <TextInput
            placeholder=""
            style={styles.textInput}
            onChangeText={formik.handleChange('username')}
            value={formik.values.username}
            onBlur={formik.handleBlur('username')}
          />
          {formik.touched.username && formik.errors.username ? (
            <Text style={styles.errorMes}>{formik.errors.username}</Text>
          ) : null}
          <Text style={styles.label}>Password</Text>
          <TextInput
            placeholder=""
            style={styles.textInput}
            secureTextEntry={true}
            onChangeText={formik.handleChange('password')}
            value={formik.values.password}
            onBlur={formik.handleBlur('password')}
          />
          {formik.touched.password && formik.errors.password ? (
            <Text style={styles.errorMes}>{formik.errors.password}</Text>
          ) : null}

          <View style={{marginVertical: 20}}>
            <Button
              title="Login"
              onPress={formik.handleSubmit}
              disabled={loading}></Button>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  label: {
    marginTop: 10,
    fontWeight: 'bold',
    color: '#999',
  },
  textInput: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 5,
    color: '#555',
    padding: 3,
    paddingHorizontal: 5,
  },
  errorMes: {
    color: 'red',
  },
});

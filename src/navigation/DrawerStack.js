import * as React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Category from '../screen/Category';
import AddCreditDebit from '../screen/AddCreditDebit';
import HomeScreen from '../screen/HomeScreen';

const Drawer = createDrawerNavigator();

export default function DrawerStack() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          unmountOnBlur: true,
        }}
      />
      <Drawer.Screen
        name="Category"
        component={Category}
        options={{
          unmountOnBlur: true,
        }}
      />
    </Drawer.Navigator>
  );
}

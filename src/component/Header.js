import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';

export default function Header({title}) {
  const navigation = useNavigation();

  return (
    <View
      style={{
        height: 50,
        backgroundColor: '#2194f3',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <TouchableOpacity
        style={{
          position: 'absolute',
          left: 15,
          height: 50,
          justifyContent: 'center',
        }}
        onPress={() => {
          navigation.openDrawer();
        }}>
        <MIcon name="menu" style={{fontSize: 35, color: '#fff'}}></MIcon>
      </TouchableOpacity>
      <Text style={{color: '#fff', fontSize: 20}}>{title}</Text>
    </View>
  );
}

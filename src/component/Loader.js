import React from 'react';
import {ActivityIndicator} from 'react-native';

function Loader() {
  return (
    <ActivityIndicator
      size="large"
      color="#2196F3"
      style={{
        position: 'absolute',
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 99999,
      }}
    />
  );
}

export default Loader;

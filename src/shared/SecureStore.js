import EncryptedStorage from 'react-native-encrypted-storage';

export const storeUserSession = async (key, data) => {
  try {
    await EncryptedStorage.setItem(key, JSON.stringify(data));

    return true;
  } catch (error) {
    return false;
  }
};

export const retrieveUserSession = async key => {
  try {
    const session = await EncryptedStorage.getItem(key);

    if (session !== undefined) {
      return session;
      // Congrats! You've just retrieved your first value!
    }
  } catch (error) {
    // There was an error on the native side

    return null;
  }
};

export const removeUserSession = async key => {
  try {
    await EncryptedStorage.removeItem(key);
    // Congrats! You've just removed your first value!
    return true;
  } catch (error) {
    // There was an error on the native side
    return false;
  }
};

export const clearStorage = async () => {
  try {
    await EncryptedStorage.clear();

    return true;
  } catch (error) {
    return false;
  }
};
